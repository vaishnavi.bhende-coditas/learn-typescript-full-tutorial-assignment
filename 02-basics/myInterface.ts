interface User {
  readonly dbId: number
  email: string,
  userId: number,
  googleId?: string
  // startTrail: () => string
  startTrail(): string
  getCoupon(couponname: string, value: number): number
}

interface User {
  githubToken: string
}

interface Admin extends User {
  role: "admin" | "ta" | "learner"
}

const vaishnavi: Admin = { dbId: 22, email: "v@v.com", userId: 2211,
role: "admin",
githubToken: "github",
startTrail: () => {
  return "trail started"
},
getCoupon: (name: "vaishnavi58", off: 10) => {
  return 10
}
}
vaishnavi.email = "v@vb.com"
// hitesh.dbId = 33