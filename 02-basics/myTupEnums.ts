// const user: (string | number)[] = [1, "hc"]
let tUser: [string, number, boolean]

tUser = ["vb", 131, true]

let rgb: [number, number, number] = [255, 123, 112]

type User = [number, string]

const newUser: User = [112, "example@google.com"]

newUser[1] = "vb.com"
// newUser.push(true)

export {}